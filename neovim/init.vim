:set number
syntax on
set mouse=a
set showtabline=2
set laststatus=0

hi TabLine ctermbg=NONE ctermfg=lightblue cterm=bold
hi TabLineFill ctermbg=NONE ctermfg=darkmagenta cterm=bold
hi TabLineSel ctermbg=NONE ctermfg=165 cterm=bold,underline
hi Title ctermbg=NONE ctermfg=darkyellow cterm=NONE

" desabilitar setinhas
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" substitutos do <esc>
inoremap jk <ESC>
inoremap kj <ESC>
