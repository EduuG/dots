install dependencies
~~~~~~~~~~~~~~~~~~~~

run this command in the directory ``searx/static/themes/forest``

``npm install``

compile sources
~~~~~~~~~~~~~~~

run this command in the directory ``searx/static/themes/forest``

``grunt``

or in the root directory:

``make grunt``
